# frozen_string_literal: true

require 'spec_helper'
allow(Time).to receive(:now).and_return('2018-01-01 10:00'.to_time)
