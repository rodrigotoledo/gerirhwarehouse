# frozen_string_literal: true

require 'rails_helper'
Capybara.javascript_driver = :webkit

RSpec.feature 'CRUD products' do
  before do
    user = FactoryBot.create(:user)
    login_as(user, scope: :user)
  end
  scenario 'List product with success' do
    FactoryBot.create(:product, name: 'Novo Material')
    visit '/'

    click_link 'Lista de Materiais'

    expect(page.current_path).to eq(products_path)
    expect(page).to have_content('Novo Material')
  end

  scenario 'Add material with success' do
    visit '/'

    click_link 'Adicionar Material'

    fill_in 'Nome do material', with: 'iMac'

    click_button 'Criar Material'
    expect(page.current_path).to eq(edit_product_path(Product.last.id))
    expect(page).to have_content('O Produto foi criado com sucesso.')
  end

  scenario 'Add material with empty name: without success' do
    visit '/'

    click_link 'Adicionar Material'

    fill_in 'Nome do material', with: ''

    click_button 'Criar Material'
    expect(page.current_path).to eq(products_path)
    expect(page).to have_content('Nome do material não pode ficar em branco')
  end

  scenario 'Add material with duplicated name: without sucess' do
    product = FactoryBot.create(:product, name: 'Produto a ser duplicado')
    visit '/'

    click_link 'Adicionar Material'

    fill_in 'Nome do material', with: 'Produto a ser duplicado'

    click_button 'Criar Material'
    expect(page.current_path).to eq(products_path)
    expect(page).to have_content('Nome do material já está em uso')
  end

  scenario 'Update material with success', js: true do
    product = FactoryBot.create(:product, name: 'Produto a ser atualizado')
    visit '/'

    find("a[href='#{edit_product_path(product.id)}']").click

    expect(page).to have_content('Alterando Material: 0 produto(s) atualmente')

    fill_in 'Nome do material', with: 'Mac Mini'

    click_button 'Atualizar Material'

    expect(page.current_path).to eq(edit_product_path(product.id))
    expect(page).to have_content('O Produto foi atualizado com sucesso.')
  end

  scenario 'Update material with empty name: without sucess' do
    product = FactoryBot.create(:product, name: 'Produto a ser atualizado sem nome')
    visit '/'

    find("a[href='#{edit_product_path(product.id)}']").click

    fill_in 'Nome do material', with: ''

    click_button 'Atualizar Material'
    expect(page.current_path).to eq(product_path(product.id))
    expect(page).to have_content('Nome do material não pode ficar em branco')
  end

  scenario 'Update material with duplicated name: without sucess' do
    product = FactoryBot.create(:product, name: 'Produto a ser atualizado com nome duplicado')
    duplicated_product = FactoryBot.create(:product)
    visit '/'

    find("a[href='#{edit_product_path(product.id)}']").click

    fill_in 'Nome do material', with: duplicated_product.name

    click_button 'Atualizar Material'
    expect(page.current_path).to eq(product_path(product.id))
    expect(page).to have_content('Nome do material já está em uso')
  end

  scenario 'Remove material with success', js: true do
    product = FactoryBot.create(:product, name: 'Produto a ser removido')
    visit '/'

    expect(page).to have_content('Produto a ser removido')

    accept_confirm do
      find("a[href='#{product_path(product.id)}']").click
    end

    expect(page).not_to have_content('Produto a ser removido')
  end
end
