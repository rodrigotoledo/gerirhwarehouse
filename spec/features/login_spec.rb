# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Login scenario' do
  scenario 'User login with success' do
    user = FactoryBot.create(:user)
    login_as(user, scope: :user)
    visit '/'

    expect(page.current_path).to eq(root_path)
  end

  scenario 'User login without success' do
    visit '/'

    expect(page.current_path).to eq(new_user_session_path)
  end
end
