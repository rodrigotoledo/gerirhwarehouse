# frozen_string_literal: true

require 'rails_helper'
Capybara.javascript_driver = :webkit

RSpec.feature 'CRUD users' do
  scenario 'List users with success' do
    @user = FactoryBot.create(:user)
    login_as(@user, scope: :user)
    visit '/'
    FactoryBot.create(:user, name: 'Rodrigo Toledo', email: 'rodrigo@rtoledo.inf.br')
    click_link 'Lista de Usuários'

    expect(page.current_path).to eq(control_users_path)
    expect(page).to have_content('Rodrigo Toledo')
  end

  scenario 'Add user with sucess' do
    @user = FactoryBot.create(:user)
    login_as(@user, scope: :user)
    visit '/'

    click_link 'Adicionar Usuário'

    fill_in 'Nome', with: 'Gislene'
    fill_in 'Email', with: 'gi.toneli@gmail.com'
    fill_in 'Senha', with: '123456#'
    fill_in 'Confirmação da senha', with: '123456#'

    click_button 'Criar Usuário'
    expect(page.current_path).to eq(control_users_path)
    expect(page).to have_content('O Usuário foi criado com sucesso.')
  end

  scenario 'Update user with success', js: true do
    @user = FactoryBot.create(:user)
    login_as(@user, scope: :user)
    user = FactoryBot.create(:user)
    visit '/'
    click_link 'Lista de Usuários'

    find("a[href='#{edit_control_user_path(user.id)}']").click

    expect(page).to have_content('Alterando Usuário: 0 alterações realizadas')

    fill_in 'Nome', with: 'Gislene'
    fill_in 'Email', with: 'gi.toneli@gmail.com'
    fill_in 'Senha', with: '123456#'
    fill_in 'Confirmação da senha', with: '123456#'

    click_button 'Atualizar Usuário'

    expect(page.current_path).to eq(control_users_path)
    expect(page).to have_content('O Usuário foi atualizado com sucesso.')
  end

  scenario 'Update logged user with success', js: true do
    @user = FactoryBot.create(:user)
    login_as(@user, scope: :user)
    visit '/'
    click_link 'Lista de Usuários'

    find("a[href='#{edit_control_user_path(@user.id)}']").click

    expect(page).to have_content('Alterando Usuário: 0 alterações realizadas')

    fill_in 'Senha', with: '123456#'
    fill_in 'Confirmação da senha', with: '123456#'

    click_button 'Atualizar Usuário'

    expect(page.current_path).to eq(new_user_session_path)
    expect(page).to have_content('Entre com suas credenciais')
  end

  scenario 'Update user with empty data: without sucess' do
    @user = FactoryBot.create(:user)
    login_as(@user, scope: :user)
    visit '/'

    click_link 'Adicionar Usuário'

    fill_in 'Nome', with: ''
    fill_in 'Email', with: ''
    fill_in 'Senha', with: ''
    fill_in 'Confirmação da senha', with: ''

    click_button 'Criar Usuário'
    expect(page.current_path).to eq(control_users_path)
    expect(page).to have_content('Nome não pode ficar em branco')
    expect(page).to have_content('Email não pode ficar em branco')
    expect(page).to have_content('Senha não pode ficar em branco')
  end

  scenario 'Update user with duplicated data with devise scenarios: without sucess' do
    @user = FactoryBot.create(:user)
    login_as(@user, scope: :user)
    user = FactoryBot.create(:user)
    user_created = FactoryBot.create(:user)
    visit '/'
    click_link 'Lista de Usuários'

    find("a[href='#{edit_control_user_path(user.id)}']").click

    fill_in 'Email', with: user_created.email

    click_button 'Atualizar Usuário'
    expect(page.current_path).to eq(control_user_path(user.id))
    expect(page).to have_content('Email já está em uso')
    expect(page).to have_content('Senha não pode ficar em branco')
  end

  scenario 'Update user with confirmation password wrong: without sucess' do
    @user = FactoryBot.create(:user)
    login_as(@user, scope: :user)
    user = FactoryBot.create(:user)
    visit '/'
    click_link 'Lista de Usuários'

    find("a[href='#{edit_control_user_path(user.id)}']").click

    fill_in 'Senha', with: '123456#'
    fill_in 'Confirmação da senha', with: '1234567#'

    click_button 'Atualizar Usuário'
    expect(page.current_path).to eq(control_user_path(user.id))
    expect(page).to have_content('Confirmação da senha não é igual a Senha')
  end

  scenario 'Update user with password small: without sucess' do
    @user = FactoryBot.create(:user)
    login_as(@user, scope: :user)
    user = FactoryBot.create(:user)
    visit '/'
    click_link 'Lista de Usuários'

    find("a[href='#{edit_control_user_path(user.id)}']").click

    fill_in 'Senha', with: '123'
    fill_in 'Confirmação da senha', with: '123'

    click_button 'Atualizar Usuário'
    expect(page.current_path).to eq(control_user_path(user.id))
    expect(page).to have_content('Senha é muito curto (mínimo: 6 caracteres)')
  end
end
