# frozen_string_literal: true

require 'rails_helper'
Capybara.javascript_driver = :webkit

RSpec.feature 'MANAGE product_logs' do
  before do
    user = FactoryBot.create(:user)
    login_as(user, scope: :user)
  end

  scenario 'Dont show product remove link because isnt inside in valid time' do
    allow(Time).to receive(:now).and_return('2018-01-01 06:00'.to_time)

    FactoryBot.create(:product_log, product: FactoryBot.create(:product, name: 'Novo Material'))
    visit '/'

    click_link 'Lista de Materiais'

    expect(page).to have_content('Novo Material')
    expect(page).not_to have_content('Diminuir')
  end

  scenario 'Dont show product remove link because the product dont have quantity' do
    allow(Time).to receive(:now).and_return('2018-01-01 12:00'.to_time)

    FactoryBot.create(:product_log, quantity: 0, product: FactoryBot.create(:product, name: 'Novo Material'))
    visit '/'

    click_link 'Lista de Materiais'

    expect(page).to have_content('Novo Material')
    expect(page).not_to have_content('Diminuir')
  end

  scenario 'Protect from remove quantity because isnt in valid time' do
    allow(Time).to receive(:now).and_return('2018-01-01 06:00'.to_time)

    product_log = FactoryBot.create(:product_log, product: FactoryBot.create(:product, name: 'Novo Material'))
    visit product_remove_quantity_path(product_log.product_id)

    expect(page).to have_content('Fora do horário permitido')
  end

  scenario 'Add product log from product list' do
    allow(Time).to receive(:now).and_return('2018-01-01 12:00'.to_time)

    product_log = FactoryBot.create(:product_log, product: FactoryBot.create(:product, name: 'Novo Material'))
    visit '/'

    click_link 'Lista de Materiais'
    find("a[href='#{product_add_quantity_path(product_log.product_id)}']").click

    expect(page).to have_content('Quantidade aumentada')
  end

  scenario 'Remove product log from product list' do
    allow(Time).to receive(:now).and_return('2018-01-01 12:00'.to_time)

    product_log = FactoryBot.create(:product_log, product: FactoryBot.create(:product, name: 'Novo Material'))
    visit '/'

    click_link 'Lista de Materiais'
    find("a[href='#{product_remove_quantity_path(product_log.product_id)}']").click

    expect(page).to have_content('Quantidade diminuída')
    expect(page.current_path).to eq(products_path)
  end

  scenario 'Remove product log from product edit' do
  end
end
