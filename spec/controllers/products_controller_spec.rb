# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ProductsController do
  describe 'Protect remove log' do
    before do
      user = FactoryBot.create(:user)
      login_as(user, scope: :user)
    end
    it 'redirect when access without business time' do
      allow(Time).to receive(:now).and_return('2018-01-01 04:00'.to_time)
      product_log = FactoryBot.create(:product_log)
      get :remove_quantity, params: { product_id: product_log.product_id }
    end
  end
end
