# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    name 'Usuário GeriRH'
    email { generate(:email) }
    password 'asdqwe123'
    password_confirmation 'asdqwe123'
  end

  sequence(:email) do
    gen = "user_#{rand(1000)}@factory.com"
    gen = "user_#{rand(1000)}@factory.com" while User.where(email: gen).exists?
    gen
  end
end
