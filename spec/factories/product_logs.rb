# frozen_string_literal: true

FactoryBot.define do
  factory :product_log do
    user
    product
    quantity 1
  end
end
