# frozen_string_literal: true

FactoryBot.define do
  factory :product do
    name { generate(:name) }
  end

  sequence(:name) do
    gen = "product_#{rand(1000)}"
    gen = "product_#{rand(1000)}" while Product.where(name: gen).exists?
    gen
  end
end
