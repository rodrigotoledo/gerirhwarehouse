# Install informations

```
Dependencies {
    rvm
    postgresql
    for tests
        xcode
        qmake
}
```


### Instructions to install

``rails db:drop db:create db:migrate db:seed``

To access the system use the informations that are in db/seeds.rb

Run

``sudo xcode-select -r``

``bundle``


### Instructions for Test

``brew install qt@5.5``

``echo 'export PATH="$(brew --prefix qt@5.5)/bin:$PATH"' >> ~/.bashrc``


### Commands for test

This will generate a new coverage for project - Press ENTER

``guard``