# frozen_string_literal: true

class CreateProductLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :product_logs do |t|
      t.references :user, foreign_key: true
      t.references :product, foreign_key: true
      t.integer :quantity

      t.timestamps
    end
  end
end
