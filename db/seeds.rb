# frozen_string_literal: true

100.times do |i|
  Product.create(name: "Produto #{i}")
end

User.create(name: 'Rodrigo Toledo', email: 'rodrigo@rtoledo.inf.br', password: 'asdqwe123', password_confirmation: 'asdqwe123')
