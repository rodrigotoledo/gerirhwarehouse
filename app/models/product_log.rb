# frozen_string_literal: true

class ProductLog < ApplicationRecord
  belongs_to :user
  belongs_to :product

  def add?
    quantity > 0
  end
end
