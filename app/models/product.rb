# frozen_string_literal: true

class Product < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  has_many :product_logs

  def amount
    product_logs.sum(:quantity)
  end

  def add_quantity(user_id, quantity = 1)
    product_log = product_logs.build(user_id: user_id, quantity: quantity)
    product_log.save
  end

  def remove_quantity(user_id, quantity = -1)
    product_log = product_logs.build(user_id: user_id, quantity: quantity)
    product_log.save
  end
end
