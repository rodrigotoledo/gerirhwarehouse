# frozen_string_literal: true

module ProductsHelper
  def can_add_product_log?(product)
    if product.new_record?
      false
    else
      true
    end
  end

  def can_remove_product_log?(product)
    if !Time.now.during_business_hours? || product.new_record?
      false
    else
      product.amount > 0
    end
  end
end
