# frozen_string_literal: true

BusinessTime::Config.work_week = %i[sun mon tue wed thu fry sat]
BusinessTime::Config.beginning_of_workday = '09:00 am'
BusinessTime::Config.end_of_workday = '18:00 pm'
