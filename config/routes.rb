# frozen_string_literal: true

Rails.application.routes.draw do
  resources :products, path: 'materiais' do
    get :add_quantity
    get :remove_quantity
  end
  resources :control_users, path: 'usuarios', only: %i[index new create edit update destroy]
  devise_for :users
  devise_scope :user do
    root to: 'products#index'
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
